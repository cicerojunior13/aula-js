
// EXERCICIO 1
document.write('<h1>Exercicio 1</h1>');
let time = 3690;
var hours = Math.floor(time / 3600);
time = time - hours * 3600;
var minutes = Math.floor(time / 60);
var seconds = time - minutes * 60;
document.write(hours + 'h ' + minutes + 'm ' + seconds + 's');

document.write('<hr>');

// EXERCICIO 2
document.write('<h1>Exercicio 2</h1>');
let n = prompt("Digite um valor inteiro."); 
let interval= [0,25,50,75,100];
document.write(getInterval(n));

function getInterval(n) {
    for (let i = 0; i<interval.length - 1; i++){
        if (n > interval[i] && n <= interval[i + 1]){
            return "O numero esta entre "+interval[i]+" e "+interval[i + 1];
        }
    }
    return "O numero está fora do intervalo";
}

document.write('<hr>');
// EXERCICIO 3
document.write('<h1>Exercicio 3</h1>');
let traveltime = prompt("Qual o tempo gasto na viagem.");
let speed = prompt("Qual a velocidade.");
let fuel = Math.ceil(traveltime * speed / 12);
document.write("O consumo foi de " + fuel + "L");